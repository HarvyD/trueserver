package server.config;




import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.*;

@Configuration
@EnableWebMvc
@ComponentScan("server")
public class WebConfig extends WebMvcConfigurerAdapter{
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {


        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        List<MediaType> types = new ArrayList<MediaType>();
        types.add(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
        //types.add(MediaType.valueOf(MediaType.TEXT_XML_VALUE));
        converter.setObjectMapper(new ObjectMapper());
        converter.setSupportedMediaTypes(types);
        converters.add(converter);
    }
}
