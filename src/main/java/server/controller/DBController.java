package server.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import server.entity.Message;
import server.service.MessageService;

import java.util.List;


@RestController

public class DBController {

    @Autowired
    private MessageService service;

    @RequestMapping(value="/server/get", method = RequestMethod.GET)
    @ResponseBody
    public List<Message> getAllMessages() {
        return service.getAll();
    }
    @RequestMapping(value="/server/get/by/id/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List <Message> getMessage(@PathVariable long id) {
        return service.getById(id);
    }
    @RequestMapping(value = "/server/post/", method = RequestMethod.POST)
    @ResponseBody
    public Message saveMessage(@RequestBody Message message) {
        return service.save(message);
    }

    @RequestMapping(value = "/server/delete/by/id/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable long id) {
        service.remove(id);
    }
}
