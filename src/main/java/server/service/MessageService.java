package server.service;

import server.entity.Message;

import java.awt.*;
import java.util.List;

public interface MessageService {
    List<Message> getAll();
    List<Message> getById(long id);
    Message save(Message remind);
    String remove(long id);
}
