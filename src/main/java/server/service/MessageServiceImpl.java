package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.entity.Message;
import server.repository.MessageRepository;

import java.awt.*;
import java.util.Collections;
import java.util.List;


@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageRepository repository;

    public List<Message> getAll() {
        return repository.findAll();
    }

    public List<Message> getById(long id) {
        return repository.findAllById(Collections.singleton(id));
    }

    public Message save(Message remind) {
        return repository.saveAndFlush(remind);
    }

    public String remove(long id) {
        repository.deleteById(id);
        return "Message with id " + id + " was successfully deleted.";
    }
}
