package server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import server.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {

}
